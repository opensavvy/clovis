plugins {
	kotlin("jvm")
}

dependencies {
	api(KotlinX.coroutines.core)

	// Apache Cassandra drivers, by DataStax
	api("com.datastax.oss:java-driver-core:_")

	implementation("opensavvy:logger:_")

	testImplementation(projects.utils.test)
}
