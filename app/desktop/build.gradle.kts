plugins {
	kotlin("jvm")
}

dependencies {
	api(projects.app.components)

	implementation("opensavvy:logger:_")
	testImplementation(projects.utils.test)
}
