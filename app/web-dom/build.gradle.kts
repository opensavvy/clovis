plugins {
	kotlin("js")
}

kotlin {
	js {
		browser()
	}
}

dependencies {
	api(projects.app.components)

	implementation("opensavvy:logger:_")
	testImplementation(projects.utils.test)
}
