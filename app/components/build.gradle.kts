@file:Suppress("UNUSED_VARIABLE")

plugins {
	kotlin("multiplatform")
	id("org.jetbrains.compose")
}

kotlin {
	jvm()
	js {
		browser()
	}

	sourceSets {
		val commonMain by getting {
			dependencies {
				api(projects.core)

				// Compose
				api(compose.runtime)

				implementation("opensavvy:logger:_")
			}
		}

		val commonTest by getting {
			dependencies {
				implementation(projects.utils.test)
			}
		}
	}
}
