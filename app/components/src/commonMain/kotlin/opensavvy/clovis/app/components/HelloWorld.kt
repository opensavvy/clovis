package opensavvy.clovis.app.components

import androidx.compose.runtime.Composable

@Composable
fun HelloWorld() {
	println("Hello world!")
}
