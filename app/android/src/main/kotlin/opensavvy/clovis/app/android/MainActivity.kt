package opensavvy.clovis.app.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import clovis.app.android.ui.theme.TestTheme

class MainActivity : ComponentActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent {
			TestTheme {
				// A surface container using the 'background' color from the theme
				Surface(modifier = Modifier.fillMaxSize(),
				        color = MaterialTheme.colorScheme.background) {
					Greeting()
				}
			}
		}
	}
}

@Composable
fun Greeting() {
	var counter by remember { mutableStateOf(0) }

	Column {
		Text(text = "Count: $counter!")

		Button(onClick = { counter++ }) {
			Text(text = "Increase")
		}
	}
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
	TestTheme {
		Greeting()
	}
}
