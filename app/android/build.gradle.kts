import de.fayard.refreshVersions.core.versionFor

plugins {
	id("com.android.application")
	kotlin("android")
}

android {
	namespace = "opensavvy.clovis.app.android"
	compileSdk = 33

	defaultConfig {
		applicationId = "opensavvy.clovis.app.android"
		minSdk = 24
		targetSdk = 33
		versionCode = 1
		versionName = "1.0"

		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
		vectorDrawables {
			useSupportLibrary = true
		}
	}

	buildTypes {
		getByName("release") {
			isMinifyEnabled = true
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
		}
	}

	compileOptions {
		sourceCompatibility(JavaVersion.VERSION_1_8)
		targetCompatibility(JavaVersion.VERSION_1_8)
	}

	kotlinOptions {
		jvmTarget = "1.8"
	}

	buildFeatures {
		compose = true
	}

	composeOptions {
		kotlinCompilerExtensionVersion = versionFor("version.androidx.compose.compiler")
	}

	buildToolsVersion = "30.0.3"
}

dependencies {
	implementation(AndroidX.core.ktx)
	implementation(AndroidX.lifecycle.runtime.ktx)
	implementation(AndroidX.activity.compose)
	implementation(AndroidX.compose.ui)
	implementation(AndroidX.compose.ui.toolingPreview)
	implementation(AndroidX.compose.material3)

	testImplementation(Testing.junit4)

	androidTestImplementation(AndroidX.test.ext.junit)
	androidTestImplementation(AndroidX.test.espresso.core)
	androidTestImplementation(AndroidX.compose.ui.testJunit4)
	debugImplementation(AndroidX.compose.ui.tooling)
	debugImplementation(AndroidX.compose.ui.testManifest)
}
