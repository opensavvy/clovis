# CLOVIS: Contribution guide

## Overview

The CLOVIS project can be built on all major operating system via Gradle.
In UNIX systems (Linux, MacOS…), Gradle is invoked via `./gradlew <options>`.
In Windows systems, Gradle is invoked via `gradlew.bat <options>`.
In the rest of this document, we assume a UNIX system.
Gradle requires having Java installed.

The CLOVIS project depends on external services (databases…).
We provide configuration to install them via Docker Compose, but you may install them in any way you wish.
To start the development environment, run:

```shell
docker-compose deployment/docker-compose.yml -d
```

Once it has started, you can use the following commands:

```shell
# Start the backend
./gradlew backend:run

# Start the desktop app
./gradlew app:run

# Start the web app
./gradlew app:jsBrowserRun
# (add the --continuous flag for automatic reload) 
```

The Android app follows the normal Android conventions and lives in the module `app.android`.

To execute the various tests:

```shell
# Builds everything and runs all tests
# Requires the database to be available and the backend to be running
./gradlew build
```

For the full list of tasks, execute `./gradlew <module>:tasks` (e.g. `./gradlew core:tasks`).

## Documentation

To generate the documentation:

```shell
# Will be generated in build/dokka/htmlMultiModule
./gradlew dokkaHtmlMultiModule
```

The latest version of the documentation [is also available here](https://opensavvy.gitlab.io/clovis/documentation/).

## Project structure

The CLOVIS project is split into multiple modules, each available as a directory.
Detailed information about each module is available in their documentation.
Here is a quick overview:

- `core` consists of the various core APIs, the underlying objects under the whole project,
- `backend` contains the implementation of the CLOVIS server,
- `app` contains the implementation of the Desktop and Web apps, `app.android` contains the implementation of the
  Android app,
- `database` and `database-server` are a first-party implementation of the CLOVIS APIs,
- `remote.common`, `remote.client` and `remote.server` handle transparently sharing information between a client and a
  backend, so the client can use remote providers as if they were local (this enables most API to be used both
  client-side or server-side),
- `utils` are the various utilities that were required to implement the various other modules:
  - `utils.logger` is Kotlin Multiplatform bare-bones logger implementation,
  - `utils.test` is a collection of utilities for Kotlin Multiplatform testing with coroutines,
  - `utils.ui` is a UI framework based on Jetpack Compose and Compose Multiplatform,
  - `utils.database` is a Kotlin/JVM driver to access Cassandra databases, using coroutines.

As a developer who wants to use or contribute to the CLOVIS project, we recommend you start by familiarizing yourself
with the concepts of `Ref` and `Provider` (in `core`).
