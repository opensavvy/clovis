@file:Suppress("UNUSED_VARIABLE")

plugins {
	kotlin("multiplatform")
}

kotlin {
	jvm()

	sourceSets {
		val commonMain by getting {
			dependencies {
				api(projects.implementations.self)

				implementation("opensavvy:logger:_")
			}
		}

		val commonTest by getting {
			dependencies {
				implementation(projects.utils.test)
			}
		}
	}
}
