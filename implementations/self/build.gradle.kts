@file:Suppress("UNUSED_VARIABLE")

plugins {
	kotlin("multiplatform")
}

kotlin {
	jvm()
	js {
		browser()
	}

	sourceSets {
		val commonMain by getting {
			dependencies {
				api(projects.core)
				api("opensavvy:spine:_")

				implementation("opensavvy:logger:_")
			}
		}

		val commonTest by getting {
			dependencies {
				implementation(projects.utils.test)
			}
		}
	}
}
