package opensavvy.clovis.core.users

import opensavvy.backbone.Backbone

/**
 * An account.
 */
data class User(
	/**
	 * The user's profile.
	 */
	val profile: Person.Ref,
) {

	interface Ref : opensavvy.backbone.Ref<User> {
		override val backbone: Bone
	}

	interface Bone : Backbone<User>
}
