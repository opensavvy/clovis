package opensavvy.clovis.core.users

import opensavvy.backbone.Backbone

/**
 * A person's profile.
 *
 * A person may or may not be a [User] of the CLOVIS project.
 * For example, a user's contacts may appear as people even if they do not have their own account.
 */
data class Person(
	val fullName: String,
) {

	interface Ref : opensavvy.backbone.Ref<Person> {
		override val backbone: Bone
	}

	interface Bone : Backbone<Person>
}
