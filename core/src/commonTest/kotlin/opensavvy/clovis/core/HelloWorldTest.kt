package opensavvy.clovis.core

import kotlin.js.JsName
import kotlin.test.Test
import kotlin.test.assertEquals

class HelloWorldTest {

	@Test
	@JsName("helloWorld")
	fun `hello world`() {
		assertEquals("Hello World!", "Hello" + ' ' + "World!")
	}
}
