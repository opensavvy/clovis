plugins {
	kotlin("jvm")
}

dependencies {
	api(projects.core)

	implementation("opensavvy:logger:_")

	testImplementation(projects.utils.test)
}
