# CLOVIS: A Personal Organizer

> **Note.**
> The development is currently in its very early stages.
> This README represents the goals of the project, not the current functionalities.

CLOVIS is an open source multiplatform software suite that combines many aspects of your life in a simple dashboard, so
you can easily plan and prioritize your future.
CLOVIS is also a collection of libraries written to make this goal a reality: from a framework for data management, to a
database driver, to a set of third-party implementations.

## CLOVIS: A multiplatform app

CLOVIS is a Kotlin/Multiplatform app based on Jetpack Compose, available on the Web, on Android and on the Desktop.
The CLOVIS app lets users track information from various services into a single interface.
It can connect to various third-party information providers, as well as to the CLOVIS backend, our first-party
implementation of our various APIs.

## CLOVIS: A framework for software architecture

CLOVIS is a way of organizing your domain objects into a cohesive API that automatically offers caching and is based on
immutable data structures for easy integration with reactive UI libraries.

Domain objects can implement different sets of features, so your code can transparently deal with heterogeneous data
from services that expose different APIs.
Domain objects remain as simple as possible to enable easy addition.

CLOVIS provides a system for API implementations to be used remotely transparently, so you can implement your domain
locally on the server, and use it client-side as if it was local to the client—or let the user decide between running an
implementation client-side (for privacy) or server-side (for data syncing) with no added code on your side.

## CLOVIS: A single API for multiple implementations

CLOVIS provides various first and third-party implementations of various day-to-day life needs, like todo lists,
calendar events and financial tracking.

As a developer, you can use these implementations in your projects so you don't have to rewrite code to access
compatible APIs time and time again.
And bugs only need to be fixed once for everyone.

## CLOVIS: A collection of utilities

To achieve our goals, we've had to create many libraries.
Of course, we've open sourced them all, from a [simple multiplatform logger](utils.logger) to
a [coroutines-enabled Apache Cassandra driver](utils.database).

## CLOVIS: An open source project

To learn more about the implementation, the project structure or the ways you can contribute, read
the [contribution guide](CONTRIBUTING.md).
