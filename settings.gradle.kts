rootProject.name = "CLOVIS"

pluginManagement {
	repositories {
		gradlePluginPortal()
		maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
		google()
	}
}

plugins {
	id("de.fayard.refreshVersions") version "0.51.0"
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(

	"core",

	//region Products

	"app:components",
	"app:android",
	"app:desktop",
	"app:web-dom",
	"backend",

	//endregion
	//region Implementations
	//region Implementation: Self

	"implementations:self",
	"implementations:self-client",
	"implementations:self-server",

	//endregion
	//endregion
	//region Utilities

	"utils:test",
	"utils:cassandra",

	//endregion
)
